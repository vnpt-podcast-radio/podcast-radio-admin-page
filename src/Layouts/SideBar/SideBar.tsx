import React, { useState } from "react";
import { Layout, Menu, theme } from "antd";
import SideBarContent from "./SideBarContent";
import { useNavigate } from "react-router-dom";
import "./SideBar.css";

const { Sider, Content, Header } = Layout;

const SideBar = ({ children }: any) => {
    const [collapsed, setCollapsed] = useState(true);
    const navigate = useNavigate();
    const {
        token: { colorBgContainer },
    } = theme.useToken();
    const currentPath = sessionStorage.getItem("keyPath");
    return (
        <Layout hasSider>
            <Sider
                style={{
                    overflow: "auto",
                    minHeight: "100vh",
                    left: 0,
                    top: 0,
                    bottom: 0,
                }}
                collapsible
                collapsed={collapsed}
                onCollapse={(value) => setCollapsed(value)}
            >
                <div
                    style={{
                        height: 32,
                        margin: 16,
                        background: "rgba(255, 255, 255, 0.2)",
                    }}
                />

                <Menu
                    theme="dark"
                    mode="inline"
                    defaultSelectedKeys={[currentPath!]}
                    items={SideBarContent}
                    onClick={(keyPath) => {
                        navigate(`${keyPath.key}`);
                        sessionStorage.setItem(
                            "keyPath",
                            keyPath.keyPath.toString()
                        );
                    }}
                />
            </Sider>
            <Layout className="site-layout">
                <Header
                    style={{ padding: 0, background: colorBgContainer }}
                ></Header>
                <Content
                    style={{
                        overflow: "auto",
                        padding: 10,
                    }}
                >
                    {children}
                </Content>
            </Layout>
        </Layout>
    );
};

export default SideBar;

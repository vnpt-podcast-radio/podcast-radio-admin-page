import {
    AudioOutlined,
    DashboardOutlined,
    HeartOutlined,
    HomeOutlined,
    LineChartOutlined,
    UsergroupAddOutlined,
    UserOutlined,
} from "@ant-design/icons";

const SideBarContent = [
    {
        label: "Dashboard",
        key: "/dashboard",
        icon: <DashboardOutlined />,
    },
    {
        label: "Home",
        key: "/home",
        icon: <HomeOutlined />,
    },
    {
        label: "Singer",
        key: "/singer",
        icon: <AudioOutlined />,
    },
    {
        label: "Song",
        key: "/song",
        icon: <LineChartOutlined />,
    },
    {
        label: "User",
        key: "/user",
        icon: <UserOutlined />,
    },
    {
        label: "Room",
        key: "/room",
        icon: <UsergroupAddOutlined />,
    },
    {
        label: "Role",
        key: "/role",
        icon: <HeartOutlined />,
    },
];

export default SideBarContent;

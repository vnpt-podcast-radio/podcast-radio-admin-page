import React from "react";
import SideBar from "../SideBar/SideBar";

export const DefaultLayout = ({ children }: any) => {
    return <SideBar>{children}</SideBar>;
};

import React, { Fragment } from "react";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import { publicRoutes } from "./routes/routes";
import { DefaultLayout } from "./Layouts/DefaultLayout/DefaultLayout";
import { NotificationProvider } from "./components/Notifications/NotificationProvider";

function App() {
    return (
        <NotificationProvider>
            <Routes>
                {publicRoutes.map((route, key) => {
                    const Layout =
                        route.layout === null ? Fragment : DefaultLayout;
                    const Page = route.component;
                    return (
                        <Route
                            key={key}
                            path={route.path}
                            element={
                                <Layout>
                                    <Page />
                                </Layout>
                            }
                        />
                    );
                })}
            </Routes>
        </NotificationProvider>
    );
}

export default App;

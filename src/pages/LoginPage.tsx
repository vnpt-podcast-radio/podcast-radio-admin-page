import React, { useState } from "react";
import { Form, Input, Button } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { AccessToken } from "../models/AccessToken";
import { ApiResponse } from "../models/ApiResponse";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { baseUrl } from "../containts";

export const LoginPage = () => {
    const ACCESS_TOKEN = "ACCESS_TOKEN";
    const REFRESH_TOKEN = "REFRESH_TOKEN";
    const navigate = useNavigate();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const onSubmitLogin = () => {
        axios
            .post<ApiResponse<AccessToken>>(`${baseUrl}/api/user/login`, {
                email: email,
                password: password,
            })
            .then((res) => {
                console.log("Login Successfull!");
                console.log(res.data);
                sessionStorage.setItem(ACCESS_TOKEN, res.data.data.accessToken);
                sessionStorage.setItem(
                    REFRESH_TOKEN,
                    res.data.data.refreshToken
                );
                navigate("/dashboard");
                sessionStorage.setItem("keyPath", "/dashboard");
            })
            .catch((err) => {
                console.log(err);
            });
    };
    const submitForm = async (e: any) => {
        e.preventDefault();
    };

    return (
        <Form
            name="normal_login"
            className="mb-3"
            initialValues={{ remember: true }}
            style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "100vh",
            }}
        >
            <Form
                style={{
                    backgroundColor: "#f0f2f5",
                    width: "30%",
                    padding: "30px",
                    borderRadius: "10px",
                }}
                onSubmitCapture={submitForm}
            >
                <div className="h1">LOGIN</div>
                <Form.Item
                    name="username"
                    rules={[
                        { required: true, message: "Please input your Email!" },
                    ]}
                >
                    <Input
                        prefix={
                            <UserOutlined className="site-form-item-icon" />
                        }
                        placeholder="Email"
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: "Please input your Password!",
                        },
                    ]}
                >
                    <Input.Password
                        prefix={
                            <LockOutlined className="site-form-item-icon" />
                        }
                        type="password"
                        placeholder="Password"
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </Form.Item>

                <Form.Item>
                    <Button
                        type="primary"
                        htmlType="submit"
                        className="login-form-button"
                        onClick={onSubmitLogin}
                    >
                        Log in
                    </Button>
                </Form.Item>
            </Form>
        </Form>
    );
};

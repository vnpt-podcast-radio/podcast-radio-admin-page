import { EditOutlined, PlusOutlined } from "@ant-design/icons";
import { Button, Input, Modal, Table } from "antd";
import { ColumnsType } from "antd/es/table";
import React, { useCallback, useContext, useEffect, useState } from "react";
import { CenteredModal } from "../components/Modal/CenteredModal";
import { NotificationContent } from "../components/Notifications/NotificationProvider";
import { REQUEST_TYPE } from "../Enums/RequestType";
import { useApi } from "../hooks/useApi";
import { ApiResponse } from "../models/ApiResponse";
import { SingerModel } from "../models/SingerModel";
import "./index.css";

export const SingerPage = () => {
    const { openNotification } = useContext(NotificationContent);
    const [loading, setLoading] = useState(false);
    const [openModal, setOpenModal] = useState(false);
    const [openModalRename, setOpenModalRename] = useState(false);
    const [newName, setSingerNewName] = useState("");
    const [singerName, setSingerName] = useState("");
    const [singer, setSinger] = useState<SingerModel>();
    const { callApi } = useApi();
    const [singers, setSingers] = useState<SingerModel[]>([]);
    const getSingers = useCallback(() => {
        callApi<ApiResponse<SingerModel[]>>(REQUEST_TYPE.GET, "api/singers")
            .then((res) => {
                console.log(res.data.data);
                setSingers(res.data.data);
            })
            .catch((err) => {
                console.error(err);
            });
    }, [callApi]);

    const createNewSinger = () => {
        if (singerName === "") {
            return openNotification("ERROR", "Singer name is Empty!", "error");
        }
        callApi(REQUEST_TYPE.POST, "api/singer/create", {
            name: singerName,
        })
            .then(() => {
                setSingerName("");
                setOpenModal(!openModal);
                getSingers();
                openNotification("SUCCESS", "Created new Singer!", "success");
            })
            .catch((error) => {
                console.log(error);
                return openNotification("ERROR", error.message, "error");
            });
    };

    const getSinger = (id: string) => {
        callApi<ApiResponse<SingerModel>>(REQUEST_TYPE.GET, `api/singer/${id}`)
            .then((res) => {
                console.log(res.data);
                setSinger(res.data.data);
                setSingerNewName(res.data.data.name);
                setOpenModalRename(!openModalRename);
            })
            .catch((error) => {
                console.log(error);
            });
    };

    const renameSinger = (id: string) => {
        if (newName === "") {
            return openNotification(
                "ERROR",
                "New Singer name is Emty!",
                "error"
            );
        }
        callApi(REQUEST_TYPE.PUT, `api/singer/rename/${id}`, {
            name: newName,
        })
            .then((res) => {
                setSingerNewName("");
                setOpenModalRename(!openModalRename);
                getSingers();
                openNotification("SUCCESS", "Rename Singer", "success");
            })
            .catch((error) => {
                return openNotification("ERROR", error.message, "error");
            });
    };

    const singerColumn: ColumnsType<SingerModel> = [
        {
            key: "index",
            title: "Index",
            render: (text, record, index) => <strong>{index + 1}</strong>,
            align: "center",
            width: 100,
        },
        {
            key: "id",
            title: "Name",
            dataIndex: "name",
        },
        {
            title: "Rename Singer",
            render: (record) => (
                <Button
                    className="d-flex align-items-center"
                    type="primary"
                    onClick={() => getSinger(record.id)}
                >
                    <EditOutlined />
                </Button>
            ),
        },
    ];
    useEffect(() => {
        return () => {
            getSingers();
        };
    }, [getSingers]);
    return (
        <div>
            <Button
                className="d-flex align-items-center"
                type="primary"
                onClick={() => setOpenModal(!openModal)}
            >
                <PlusOutlined />
                Singer
            </Button>
            <CenteredModal
                open={openModal}
                title={"Create new Singer!"}
                onCancel={() => setOpenModal(!openModal)}
                onOk={createNewSinger}
                spinning={loading}
            >
                <Input
                    className="m-2 form-control"
                    value={newName}
                    placeholder="Singer Name"
                    onChange={(e) => setSingerName(e.target.value)}
                />
            </CenteredModal>

            <Modal
                centered
                open={openModalRename}
                title={"Rename Singer"}
                onCancel={() => setOpenModalRename(!openModalRename)}
                onOk={() => renameSinger(singer?.id!)}
            >
                <Input
                    value={newName}
                    placeholder="New Name Singer"
                    onChange={(e) => setSingerNewName(e.target.value)}
                />
            </Modal>

            <Table columns={singerColumn} dataSource={singers} />
        </div>
    );
};

import { UserAddOutlined } from "@ant-design/icons";
import { Button, Card, Input, List } from "antd";
import React, { useCallback, useEffect, useState } from "react";
import { REQUEST_TYPE } from "../Enums/RequestType";
import { useApi } from "../hooks/useApi";
import { ApiResponse } from "../models/ApiResponse";
import { UserModel } from "../models/UserModel";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { CenteredModal } from "../components/Modal/CenteredModal";
import { useNavigate } from "react-router-dom";

export const UserPage = () => {
    const [users, setUsers] = useState<UserModel[]>([]);
    const [visibleModal, setVisibelModal] = useState(false);
    const [loading, setLoading] = useState(false);
    const { callApi } = useApi();
    const getUsers = useCallback(() => {
        callApi<ApiResponse<UserModel[]>>(REQUEST_TYPE.GET, "api/users")
            .then((res) => setUsers(res.data.data))
            .catch((err) => {
                console.error(err);
            });
    }, [callApi]);

    const navigate = useNavigate();

    const onOpenModal = () => {
        setVisibelModal(true);
    };

    const onCloseModal = () => {
        setVisibelModal(false);
    };

    useEffect(() => {
        return () => {
            getUsers();
        };
    }, [getUsers]);
    return (
        <div>
            <Button
                className="d-flex align-items-center m-2"
                type="primary"
                onClick={onOpenModal}
            >
                <UserAddOutlined />
            </Button>
            <CenteredModal
                open={visibleModal}
                onCancel={onCloseModal}
                centered
                title="Add User"
                spinning={loading}
            >
                <Input type="text" className="margin" placeholder="Name" />
                <Input type="email" className="margin" placeholder="Email" />
                <Input.Password className="margin" placeholder="Password" />
                <Input className="margin" placeholder="Role" />
            </CenteredModal>

            <List
                grid={{
                    gutter: 16,
                    xs: 1,
                    sm: 2,
                    md: 4,
                    lg: 4,
                    xl: 6,
                    xxl: 3,
                }}
                dataSource={users}
                renderItem={(item) => (
                    <List.Item onClick={() => navigate(item.id)}>
                        <Card title={item.name} hoverable>
                            <p className="text-truncate">{item.phone}</p>
                            <p className="text-truncate">{item.email}</p>
                        </Card>
                    </List.Item>
                )}
            />
        </div>
    );
};

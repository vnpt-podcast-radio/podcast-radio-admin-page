import {
    GithubOutlined,
    LoadingOutlined,
    MailOutlined,
    UserOutlined,
} from "@ant-design/icons";
import { Avatar, Card, List } from "antd";
import React, { useCallback, useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { NotificationContent } from "../components/Notifications/NotificationProvider";
import { REQUEST_TYPE } from "../Enums/RequestType";
import { useApi } from "../hooks/useApi";
import { ApiResponse } from "../models/ApiResponse";
import { RoomModel } from "../models/RoomModel";
import { UserModel } from "../models/UserModel";

export const DetailUser = () => {
    const [user, setUser] = useState<UserModel>();
    const { callApi } = useApi();
    const { userId } = useParams();
    const { openNotification } = useContext(NotificationContent);

    const getUser = useCallback(() => {
        callApi<ApiResponse<UserModel>>(REQUEST_TYPE.GET, `api/user/${userId}`)
            .then((res) => {
                setUser(res.data.data);
                console.log(res.data.data);
            })
            .catch((err) => {
                console.error(err);
            });
    }, [callApi, userId]);
    const navigate = useNavigate();

    const navigateToRoom = (room: RoomModel) => {
        if (room.status) {
            return navigate(`/live/${room.id}`);
        }
        return openNotification("Room is offline", "", "warning");
    };

    useEffect(() => {
        return () => {
            getUser();
        };
    }, []);
    return (
        <div>
            <div className="card p-4 d-flex align-items-center justify-content-center">
                <Avatar
                    src={
                        user?.avatar
                            ? user.avatar
                            : "https://i.kym-cdn.com/photos/images/facebook/001/551/402/80c.jpeg"
                    }
                    size={120}
                />
                <span className="d-flex align-items-center text-danger truncate lead">
                    <UserOutlined />
                    <span className="mx-2 truncate">{user?.name}</span>
                </span>
                <span className="text-danger lead truncate d-flex align-items-center">
                    <MailOutlined />
                    <span className="mx-2 truncate">{user?.email}</span>
                </span>
                <span className="text-danger lead text-truncate d-flex align-items-center">
                    <GithubOutlined />
                    <span className="text-danger truncate mx-2">
                        {user?.role.name}
                    </span>
                </span>
            </div>
            <List
                grid={{
                    gutter: 16,
                    xs: 1,
                    sm: 2,
                    md: 4,
                    lg: 4,
                    xl: 6,
                    xxl: 3,
                }}
                dataSource={user?.rooms}
                renderItem={(value) => (
                    <List.Item onClick={() => navigateToRoom(value)}>
                        <Card
                            title={
                                <div>
                                    <span>{value.name}</span>
                                    {value.status ? (
                                        <span className="text-danger position-fixed mx-2">
                                            <LoadingOutlined />
                                        </span>
                                    ) : (
                                        ""
                                    )}
                                </div>
                            }
                            hoverable
                        >
                            <p className="text-truncate">{value.describe}</p>
                        </Card>
                    </List.Item>
                )}
            />
        </div>
    );
};

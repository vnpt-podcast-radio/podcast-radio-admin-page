import { FileAddOutlined } from "@ant-design/icons";
import { Button, Input, List, Card } from "antd";
import React, { useCallback, useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserModel } from "../models/UserModel";
import { NotificationContent } from "../components/Notifications/NotificationProvider";
import { useApi } from "../hooks/useApi";
import { ApiResponse } from "../models/ApiResponse";
import { REQUEST_TYPE } from "../Enums/RequestType";
import { RoomModel } from "../models/RoomModel";
import "bootstrap/dist/css/bootstrap.min.css";

import "./index.css";
import { CenteredModal } from "../components/Modal/CenteredModal";

export const HomePage = () => {
    const { openNotification } = useContext(NotificationContent);
    const [loading, setLoading] = useState(false);
    const [user, setUser] = useState<UserModel>();
    const { callApi } = useApi();
    const [open, setOpen] = useState(false);
    const [room, setRoom] = useState("");
    const navigate = useNavigate();
    const [roomName, setRoomName] = useState("");
    const [roomDescribe, setRoomDescribe] = useState("");
    const [rooms, setRooms] = useState<RoomModel[]>([]);

    const onClickJoinRoom = () => {
        if (room === "") {
            return openNotification("Thong bao", "Room is Empty!", "error");
        }
        navigate(`/live/${room}`);
    };

    const getAllRooms = useCallback(() => {
        callApi<ApiResponse<RoomModel[]>>(REQUEST_TYPE.GET, "api/rooms")
            .then((response) => {
                setRooms(response.data.data);
                console.log(response);
            })
            .catch((error) => {
                console.error(error);
            });
    }, [callApi]);

    const onCreateRoom = () => {
        callApi(REQUEST_TYPE.POST, "api/room/create", {
            name: roomName,
            describe: roomDescribe,
        })
            .then((res) => {
                const { roomId }: any = res.data;
                navigate(`/live/${roomId}`);
            })
            .catch((err) => {
                console.error(err);
            });
    };
    const getCurrentUser = useCallback(() => {
        callApi<ApiResponse<UserModel>>(REQUEST_TYPE.GET, "api/user/current")
            .then((res) => {
                setUser(res.data.data);
                console.log(res.data.data);
            })
            .catch((error) => {
                console.error(error);
            });
    }, [callApi]);

    const onCloseModal = () => {
        setOpen(!open);
        setRoomName("");
        setRoomDescribe("");
    };

    useEffect(() => {
        getCurrentUser();
        getAllRooms();
    }, [getAllRooms, getCurrentUser]);

    return (
        <div>
            <div className="bg-gradient p-3 d-flex flex-column align-items-center">
                <h1>Name: {user?.name}</h1>
                <h1>Email: {user?.email}</h1>
            </div>
            <div className="modal-content">
                <Input
                    className="margin form-control"
                    placeholder="Room ID"
                    value={room}
                    onChange={(e) => setRoom(e.target.value)}
                />
            </div>
            <div className="d-flex align-items-center mb-1">
                <Button type="primary" onClick={onClickJoinRoom}>
                    JOIN ROOM
                </Button>
                <Button
                    className="d-flex align-items-center m-1"
                    onClick={() => setOpen(!open)}
                    type="primary"
                >
                    <FileAddOutlined />
                </Button>
            </div>
            <List
                grid={{
                    gutter: 16,
                    xs: 1,
                    sm: 2,
                    md: 4,
                    lg: 4,
                    xl: 6,
                    xxl: 5,
                }}
                dataSource={rooms}
                renderItem={(item, index) => (
                    <List.Item
                        onClick={() => {
                            navigate(`/live/${item.id}`);
                        }}
                    >
                        <Card
                            className="justify-content-center d-flex"
                            size="small"
                            hoverable
                            type="inner"
                        >
                            <div className="d-flex flex-column align-items-center">
                                <p className="h3 text-danger text-uppercase">
                                    {item.name}
                                </p>
                                <p className="h5">{item.user.name}</p>
                                <p className="lead text-info">
                                    {item.describe}
                                </p>
                            </div>
                        </Card>
                    </List.Item>
                )}
            />

            <CenteredModal
                title={"Create new Room!"}
                open={open}
                onCancel={onCloseModal}
                onOk={onCreateRoom}
                spinning={loading}
            >
                <Input
                    className="m-2 form-control"
                    placeholder="Room Name"
                    onChange={(e) => setRoomName(e.target.value)}
                    value={roomName}
                />
                <Input
                    className="m-2 form-control"
                    placeholder="Room Desribe"
                    onChange={(e) => setRoomDescribe(e.target.value)}
                    value={roomDescribe}
                />
            </CenteredModal>
        </div>
    );
};

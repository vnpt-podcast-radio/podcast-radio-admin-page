import { UserAddOutlined } from "@ant-design/icons";
import { Button, Input } from "antd";
import React, { useContext, useState } from "react";
import { NotificationContent } from "../components/Notifications/NotificationProvider";
import { REQUEST_TYPE } from "../Enums/RequestType";
import { useApi } from "../hooks/useApi";
import "bootstrap/dist/css/bootstrap.min.css";
import { CenteredModal } from "../components/Modal/CenteredModal";

export const RolePage = () => {
    const { callApi } = useApi();
    const { openNotification } = useContext(NotificationContent);
    const [roleName, setRoleName] = useState("");
    const [openModal, setOpenModal] = useState(false);
    const [loading, setLoading] = useState(false);

    const createNewRole = () => {
        callApi(REQUEST_TYPE.POST, "/api/role/create", {
            name: roleName,
        })
            .then(() => {
                openNotification(
                    "SUCCESS",
                    "Role created successfully",
                    "success"
                );
                setOpenModal(false);
                setRoleName("");
            })
            .catch((err) => {
                openNotification("FAILED", "Role create failed", "error");
                console.error(err);
            });
    };

    const onCloseModal = () => {
        setOpenModal(false);
        setRoleName("");
    };

    const onChangeRoleName = (e: any) => {
        setRoleName(e.target.value);
        console.log(e.target.value);
    };

    return (
        <div>
            <Button
                type="primary"
                className="d-flex align-items-center"
                onClick={() => setOpenModal(!openModal)}
            >
                <UserAddOutlined />
            </Button>
            <CenteredModal
                open={openModal}
                onCancel={onCloseModal}
                onOk={createNewRole}
                title="New role"
                spinning={loading}
            >
                <Input
                    className="margin form-control"
                    placeholder="Role Name"
                    onChange={onChangeRoleName}
                    value={roleName}
                ></Input>
            </CenteredModal>
        </div>
    );
};

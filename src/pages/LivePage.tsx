import { Button, Card, Input, List, Modal } from "antd";
import React, { useCallback, useEffect, useRef, useState } from "react";
import ReactAudioPlayer from "react-audio-player";
import { useNavigate, useParams } from "react-router-dom";
import io from "socket.io-client";
import { REQUEST_TYPE } from "../Enums/RequestType";
import { useApi } from "../hooks/useApi";
import { ApiResponse } from "../models/ApiResponse";
import { RoomModel } from "../models/RoomModel";
import { SongModel } from "../models/SongModel";
import { UserModel } from "../models/UserModel";
import "bootstrap/dist/css/bootstrap.min.css";
import { ChromeOutlined, DollarOutlined } from "@ant-design/icons";
const socket = io("http://192.168.0.104:3333");

export const LivePage = () => {
    const [joined, setJoined] = useState<string[]>([]);
    const [messages, setMessages] = useState<string[]>([]);
    const [user, setUser] = useState<UserModel>();
    const [input, setInput] = useState("");
    const [modalStop, setModalStop] = useState(false);
    const { roomId } = useParams();
    const { callApi } = useApi();
    const timeGuest = useRef<ReactAudioPlayer>(null);
    const timeOwner = useRef<ReactAudioPlayer>(null);
    const [room, setRoom] = useState<RoomModel>();
    const [roomOwner, setRoomOwner] = useState(false);
    const [songs, setSongs] = useState<SongModel[]>([]);
    const navigate = useNavigate();
    const [currentSong, setCurrentSong] = useState<SongModel>();

    const getCurrentUser = useCallback(() => {
        callApi<ApiResponse<UserModel>>(REQUEST_TYPE.GET, "api/user/current")
            .then((res) => {
                checkRoomOwner(res.data.data.rooms);
                setUser(res.data.data);
                joinRoom(res.data.data.email);
            })
            .catch((error) => {
                console.error(error);
            });
    }, [callApi]);

    const fetchAllSongs = useCallback(() => {
        callApi<ApiResponse<SongModel[]>>(REQUEST_TYPE.GET, "api/songs")
            .then((res) => {
                setSongs(res.data.data);
                console.log(res);
            })
            .catch((error) => {
                console.error(error);
            });
    }, [callApi]);

    const checkRoomOwner = (rooms: RoomModel[]) => {
        const owner = rooms.find((r) => r.id === roomId);
        if (owner) {
            setRoomOwner(true);
        } else {
            setRoomOwner(false);
        }
    };

    const getCurrentRoom = useCallback(() => {
        callApi<ApiResponse<RoomModel>>(REQUEST_TYPE.GET, `api/room/${roomId}`)
            .then((res) => {
                setRoom(res.data.data);
            })
            .catch((err) => {
                console.error(err);
            });
    }, [callApi, roomId]);

    //JOIN ROOM
    const joinRoom = (email: any) => {
        const data = {
            roomId: roomId,
            email: email,
        };
        //SEND
        socket.emit("join-room", data);
        socket.on("joined", (data) => {
            sendSongInfoToServer();
            setJoined((prevState) =>
                prevState.concat(`User ${data.name} has joined the room.`)
            );
            getSongInfo();
        });
    };
    //SEND SONG INFO
    const sendSongInfoToServer = (): void => {
        const data = {
            songUrl: timeOwner.current?.audioEl.current?.src,
            currentTime: timeOwner.current?.audioEl.current?.currentTime,
            roomId: roomId,
        };
        socket.emit("song-info", data);
    };

    const getSongInfo = () => {
        if (!roomOwner) {
            socket.on("get-song-info", (data) => {
                if (timeGuest.current?.audioEl.current) {
                    timeGuest.current.audioEl.current.src = data.songUrl;
                    timeGuest.current.audioEl.current.currentTime =
                        data.currentTime;
                }
            });
        } else {
            return null;
        }
    };

    const onChangeMessage = (e: any) => {
        setInput(e.target.value);
    };
    const sendMessage = () => {
        socket.emit("chat-message", {
            userId: user?.id,
            message: input,
            email: user?.email,
        });
        setInput("");
    };
    const receiveMessage = useCallback(() => {
        socket.on("message", (data) => {
            console.log(Math.random().toString());
            setMessages((prevState) =>
                prevState.concat(`${data.email} : ${data.message}`)
            );
        });
    }, []);

    const handleChangeSongInRoom = (song: SongModel) => {
        changeCurrentSong(song);
        setCurrentSong(song);
        socket.emit("change-song-in-room", song);
    };
    const receiveSongChange = useCallback(() => {
        socket.on("event-song", (data) => {
            changeCurrentSongGuest(data);
        });
    }, []);

    const changeCurrentSong = (song: SongModel) => {
        if (timeOwner.current?.audioEl.current) {
            timeOwner.current.audioEl.current.src = song.url_song;
        }
    };

    const changeCurrentSongGuest = (song: any) => {
        console.log(song.song);
        if (timeGuest.current?.audioEl.current) {
            timeGuest.current.audioEl.current.src = song.song.url_song;
            timeGuest.current.audioEl.current.autoplay = true;
            timeGuest.current.audioEl.current.currentTime = 0;
        }
    };

    const receiveStatusSong = useCallback(() => {
        socket.on("status-song", (data) => {
            const status = data.status;
            switch (status) {
                case "play": {
                    console.log("Play" + status);
                    if (timeGuest.current?.audioEl.current) {
                        timeGuest.current.audioEl.current.play();
                    }
                    break;
                }
                case "pause": {
                    console.log("Pause" + status);
                    if (timeGuest.current?.audioEl.current) {
                        timeGuest.current.audioEl.current.pause();
                    }
                    break;
                }
                default: {
                    if (timeGuest.current?.audioEl.current) {
                        timeGuest.current.audioEl.current.play();
                    }
                    break;
                }
            }
        });
    }, []);

    const sendStatusSong = (status: string) => {
        socket.emit("send-status-song", { status });
    };

    const onPauseSong = () => {
        const status = "pause";
        sendStatusSong(status);
    };

    const onEndedSong = () => {
        const status = "end";
        sendStatusSong(status);
    };

    const onPlaySong = () => {
        const status = "play";
        sendStatusSong(status);
    };

    const onStopRoom = () => {
        socket.emit("stop-room");
    };
    const onRoomStopped = useCallback(() => {
        socket.on("room-stopped", () => {
            navigate("/home");
        });
    }, [navigate]);
    const openModalStop = () => {
        setModalStop(true);
    };

    useEffect(() => {
        return () => {
            getCurrentUser();
            fetchAllSongs();
            onRoomStopped();
            receiveStatusSong();
            receiveSongChange();
            receiveMessage();
            getCurrentRoom();
        };
    }, [
        fetchAllSongs,
        getCurrentRoom,
        getCurrentUser,
        onRoomStopped,
        receiveMessage,
        receiveSongChange,
        receiveStatusSong,
    ]);

    return (
        <div>
            <h2>Room Name : {room?.name}</h2>
            <h2>Room Owner : {room?.user.name}</h2>
            <h2>Room ID : {roomId}</h2>
            <h3>Email: {user?.email}</h3>
            <h3>Guest Name: {user?.name}</h3>
            <p>{roomOwner ? "You" : ""}</p>
            {roomOwner ? (
                <ReactAudioPlayer
                    onPlay={onPlaySong}
                    onEnded={onEndedSong}
                    onPause={onPauseSong}
                    ref={timeOwner}
                    controls
                    autoPlay={true}
                />
            ) : (
                <ReactAudioPlayer autoPlay={true} ref={timeGuest} />
            )}
            <div
                style={{
                    width: 300,
                }}
            >
                {roomOwner ? (
                    <Card
                        title={
                            <div className="d-flex align-items-center">
                                BÀI HÁT ĐANG PHÁT
                                <DollarOutlined spin />
                            </div>
                        }
                    >
                        <div>{currentSong?.name}</div>
                        {currentSong?.singers.map((value, key) => (
                            <div key={key}>{value.name}</div>
                        ))}
                    </Card>
                ) : (
                    <Card
                        title={
                            <div className="d-flex align-items-center">
                                BÀI HÁT ĐANG PHÁT Con Chó MInh
                                <DollarOutlined spin />
                            </div>
                        }
                    >
                        <div>{currentSong?.name}</div>
                        {currentSong?.singers.map((value, key) => (
                            <div key={key}>{value.name}</div>
                        ))}
                    </Card>
                )}
                <Input
                    value={input}
                    placeholder="message"
                    onChange={onChangeMessage}
                />
                <div className="mt-2">
                    <Button onClick={sendMessage} type="primary">
                        Send
                    </Button>
                    {roomOwner ? (
                        <Button className="mx-4" onClick={openModalStop} danger>
                            End
                        </Button>
                    ) : (
                        ""
                    )}
                </div>
            </div>
            {roomOwner ? (
                <List
                    dataSource={songs}
                    renderItem={(item) => (
                        <List.Item key={item.id}>
                            <List.Item.Meta
                                title={
                                    <Button
                                        onClick={() =>
                                            handleChangeSongInRoom(item)
                                        }
                                        type="text"
                                    >
                                        {item.name}
                                    </Button>
                                }
                            />
                        </List.Item>
                    )}
                />
            ) : (
                <div>DÂN THƯỜNG MÀ ĐÒI XEM DANH SÁCH NHẠC À</div>
            )}

            <div
                style={{
                    height: 200,
                    backgroundColor: "#2222",
                    borderRadius: 10,
                    padding: 10,
                    overflow: "auto",
                }}
            >
                <div>
                    {messages.map((message, index) => (
                        <li key={index}>{message}</li>
                    ))}
                    {joined.map((message, index) => (
                        <li key={index}>{message}</li>
                    ))}
                </div>
            </div>
            <Modal
                open={modalStop}
                title="You sure stop live!"
                onCancel={() => setModalStop(false)}
                onOk={onStopRoom}
                centered
            ></Modal>
        </div>
    );
};

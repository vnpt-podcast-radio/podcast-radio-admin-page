import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import { Button, Input, Table, Select, Space } from "antd";
import { ColumnsType } from "antd/es/table";
import axios from "axios";
import React, { useCallback, useContext, useEffect, useState } from "react";
import ReactAudioPlayer from "react-audio-player";
import { NotificationContent } from "../components/Notifications/NotificationProvider";
import { REQUEST_TYPE } from "../Enums/RequestType";
import { useApi } from "../hooks/useApi";
import { ApiResponse } from "../models/ApiResponse";
import { SingerModel } from "../models/SingerModel";
import { SongModel } from "../models/SongModel";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { CenteredModal } from "../components/Modal/CenteredModal";

const { Option } = Select;

export const SongPage = () => {
    const { callApi } = useApi();
    const ACCESS_TOKEN = "ACCESS_TOKEN";
    const { openNotification } = useContext(NotificationContent);
    const [singerSelect, setSingerSelect] = useState<[]>([]);
    const [file, setFile] = useState(null);
    const [openModal, setOpenModal] = useState(false);
    const [songName, setSongName] = useState("");
    const [songs, setSongs] = useState<SongModel[]>([]);
    const [loading, setLoading] = useState(false);
    const [singers, setSingers] = useState<SingerModel[]>([]);
    const getSingers = useCallback(() => {
        callApi<ApiResponse<SingerModel[]>>(REQUEST_TYPE.GET, "api/singers")
            .then((res) => {
                console.log(res.data.data);
                setSingers(res.data.data);
            })
            .catch((err) => {
                console.error(err);
            });
    }, [callApi]);

    const createNewSong = () => {
        setLoading(true);
        const formData = new FormData();
        const objectArray = singerSelect.map((item) => ({ id: item }));
        const jsonSong = {
            name: songName,
            singers: objectArray,
        };
        console.log(jsonSong);
        formData.append("file", file!);
        formData.append("data", JSON.stringify(jsonSong));
        const config = {
            headers: {
                Authorization: "Bearer " + sessionStorage.getItem(ACCESS_TOKEN),
                "content-type": "multipart/form-data",
            },
        };
        axios
            .post("http://localhost:5500/api/song/create", formData, config)
            .then(() => {
                getSongs();
                setOpenModal(!openModal);
                setLoading(false);
                openNotification("SUCCESS", "Thành Công", "success");
                cleanUp();
            })
            .catch((err) => {
                setLoading(false);
                openNotification("FAILED", err.response.data.error, "error");
            });
    };

    const onChangeFile = (e: any) => {
        setFile(e.target.files[0]);
    };
    const openModalNewSong = () => {
        getSingers();
        setOpenModal(!openModal);
    };

    const deleteSong = (id: string) => {
        callApi<ApiResponse<any>>(REQUEST_TYPE.DELETE, `api/song/${id}`)
            .then((res) => {
                getSongs();
                openNotification("SUCCESS!", res.data.message, "success");
            })
            .catch((error) => {
                openNotification("SUCCESS!", error.message, "error");
            });
    };

    const getSongs = useCallback(() => {
        callApi<ApiResponse<SongModel[]>>(REQUEST_TYPE.GET, "api/songs")
            .then((res) => {
                setSongs(res.data.data);
            })
            .catch((error) => {
                console.log(error);
            });
    }, [callApi]);

    const songColumn: ColumnsType<SongModel> = [
        {
            key: "index",
            title: "Index",
            render: (text, record, index) => <strong>{index + 1}</strong>,
            align: "center",
        },
        {
            key: "name",
            title: "Name",
            dataIndex: "name",
            width: 100,
            render: (text) => <strong className="text">{text}</strong>,
        },
        {
            key: "audio",
            title: "Audio",
            render: (record) => (
                <ReactAudioPlayer
                    style={{ height: "30px" }}
                    controls
                    src={record.url_song}
                />
            ),
            align: "center",
        },
        {
            title: "Delete",
            render: (rescord) => (
                <Button
                    className="d-flex align-items-center"
                    type="primary"
                    onClick={() => deleteSong(rescord.id)}
                >
                    <DeleteOutlined />
                </Button>
            ),
        },
    ];

    const handleSelectChange = (value: any) => {
        setSingerSelect(value);
    };
    const cleanUp = () => {
        setSongName("");
        setSingerSelect([]);
        setFile(null);
    };

    const onCloseCreateSong = () => {
        setOpenModal(!openModal);
        cleanUp();
    };

    useEffect(() => {
        return () => {
            getSongs();
        };
    }, [getSongs]);

    return (
        <div>
            <Button
                className="margin d-flex align-items-center"
                type="primary"
                onClick={openModalNewSong}
            >
                <PlusOutlined />
                Song
            </Button>
            <CenteredModal
                title={"New Song"}
                open={openModal}
                onCancel={onCloseCreateSong}
                onOk={createNewSong}
                spinning={loading}
            >
                <Select
                    className="margin form-control"
                    mode="multiple"
                    style={{ width: "100%" }}
                    placeholder="select singers"
                    optionLabelProp="label"
                    onChange={handleSelectChange}
                    value={singerSelect}
                >
                    {singers.map((value, index) => (
                        <Option value={value.id} label={value.name} key={index}>
                            <Space>
                                <span role="img" aria-label="China">
                                    {value.name}
                                </span>
                            </Space>
                        </Option>
                    ))}
                </Select>
                <Input
                    value={songName}
                    className="margin form-control"
                    placeholder="Song name"
                    onChange={(e) => setSongName(e.target.value)}
                />
                <input
                    className="margin form-control form-control-sm"
                    type={"file"}
                    onChange={(e) => onChangeFile(e)}
                    accept={"audio/mpeg, audio/wav, audio/flac, audio/ogg"}
                />
            </CenteredModal>
            <Table columns={songColumn} dataSource={songs} />
        </div>
    );
};

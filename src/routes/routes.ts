import { DashboardPage } from "../pages/DashboardPage";
import { DetailUser } from "../pages/DetailUser";
import { HomePage } from "../pages/HomePage";
import { LivePage } from "../pages/LivePage";
import { LoginPage } from "../pages/LoginPage";
import { RolePage } from "../pages/RolePage";
import { RoomPage } from "../pages/RoomPage";
import { SingerPage } from "../pages/SingerPage";
import { SongPage } from "../pages/SongPage";
import { UserPage } from "../pages/UserPage";

export const publicRoutes = [
    { path: "/", component: LoginPage, layout: null },
    { path: "/home", component: HomePage },
    { path: "/live/:roomId", component: LivePage },
    { path: "/singer", component: SingerPage },
    { path: "/dashboard", component: DashboardPage },
    { path: "/song", component: SongPage },
    { path: "/user", component: UserPage },
    { path: "/room", component: RoomPage },
    { path: "/role", component: RolePage },
    { path: "/user/:userId", component: DetailUser },
];

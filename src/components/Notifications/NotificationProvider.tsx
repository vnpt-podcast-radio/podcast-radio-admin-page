import { notification } from "antd";
import React, { createContext, useState } from "react";
type NotificationType = "success" | "info" | "warning" | "error";

export interface NotificationContextType {
  openNotification: (
    message: string,
    description?: string,
    type?: NotificationType
  ) => void;
}

export const NotificationContent = createContext<NotificationContextType>({
  openNotification: () => {},
});

export const NotificationProvider = ({ children }: any) => {
  const openNotification = (
    message: string,
    description?: string,
    type?: NotificationType
  ) => {
    notification.open({
      message,
      description,
      type,
    });
  };

  return (
    <NotificationContent.Provider value={{ openNotification }}>
      {children}
    </NotificationContent.Provider>
  );
};

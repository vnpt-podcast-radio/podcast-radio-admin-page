import { Modal, Spin } from "antd";
import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { CloseOutlined } from "@ant-design/icons";

export const CenteredModal = ({
    open,
    onCancel,
    onOk,
    children,
    title,
    spinning,
}: any) => {
    return (
        <Modal
            title={<p className="mb-2 lead">{title}</p>}
            centered
            open={open}
            onCancel={onCancel}
            onOk={onOk}
            closeIcon={
                <CloseOutlined
                    onClick={onCancel}
                    className="d-flex justify-content-center"
                />
            }
        >
            <Spin spinning={spinning} size="large">
                <div className="d-flex flex-column align-items-center pt-4">
                    {children}
                </div>
            </Spin>
        </Modal>
    );
};

export interface HomeModel {
    id: string,
    name: string,
    phone: string,
    email: string
}
import { SongModel } from "./SongModel";

export interface SingerModel {
    id: string,
    name: string,
    songs: SongModel[]
}

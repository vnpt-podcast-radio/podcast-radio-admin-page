export interface LoginModel {
    id: string,
    name: string,
    phone: string,
    email: string
}
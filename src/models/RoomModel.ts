import { UserModel } from "./UserModel";

export interface RoomModel {
    id: string;
    name: string;
    describe: string;
    userId: string;
    status: boolean;
    user: UserModel;
}

import { RoleModel } from "./RoleModel";
import { RoomModel } from "./RoomModel";

export interface UserModel {
    id: string;
    name: string;
    phone: string;
    email: string;
    rooms: RoomModel[];
    avatar: string;
    roleId: string;
    role: RoleModel;
}

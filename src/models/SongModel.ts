import { SingerModel } from "./SingerModel";

export interface SongModel {
    id: string,
    name: string,
    url_song: string,
    singers: SingerModel[]
}